class Train:
    def __init__(self, trainer, resume_from_checkpoint):
        self.trainer = trainer
        self.resume_from_checkpoint = self.__process_resume_from_checkpoint(resume_from_checkpoint)

    def call(self):
        if self.resume_from_checkpoint is None:
            result = self.trainer.train()
        else:
            result = self.trainer.train(resume_from_checkpoint = self.resume_from_checkpoint)

        return result

    def __process_resume_from_checkpoint(self, resume_from_checkpoint):
        if resume_from_checkpoint is None:
            return None
        
        if resume_from_checkpoint == 'True':
            return True

        return resume_from_checkpoint
