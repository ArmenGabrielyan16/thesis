import torch
from transformers import AutoTokenizer, VisionEncoderDecoderModel

import utils

class Inference:
  def __init__(self, decoder_model_name, model_checkpoint, decoding_strategy, max_length=32):
    self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    self.tokenizer = AutoTokenizer.from_pretrained(decoder_model_name)
    self.encoder_decoder_model = VisionEncoderDecoderModel.from_pretrained(model_checkpoint)
    self.encoder_decoder_model.to(self.device)

    self.max_length = max_length
    self.decoding_strategy = decoding_strategy

  def generate_text(self, video, encoder_model_name):
    if isinstance(video, str):
      pixel_values = utils.video2image_from_path(video, encoder_model_name)
    else:
      pixel_values = video

    if not self.tokenizer.pad_token:
      self.tokenizer.add_special_tokens({'pad_token': '[PAD]'})
      self.encoder_decoder_model.decoder.resize_token_embeddings(len(self.tokenizer))

    unsqueezed_pixel_values = pixel_values.unsqueeze(0).to(self.device)

    if self.decoding_strategy == 'beam_search':
      generated_ids = self.encoder_decoder_model.generate(
        unsqueezed_pixel_values,
        max_length=self.max_length,
        num_beams=5, 
        no_repeat_ngram_size=2,
        early_stopping=True,
      )
    else:
      generated_ids = self.encoder_decoder_model.generate(
        unsqueezed_pixel_values,
        max_length=self.max_length,
        early_stopping=True
      )

    generated_text = self.tokenizer.batch_decode(generated_ids, skip_special_tokens=True)[0]

    return generated_text
