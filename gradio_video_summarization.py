import argparse
import gradio as gr

from inference import Inference


if __name__ == '__main__':
  parser = argparse.ArgumentParser(
      description='This demo application is to demonstrate text summarization.'
  )
  parser.add_argument(
      '--model_checkpoint',
      type=str,
      required=True,
      help='Indicates checkpoint path that model should be loaded from for inference.'
  )
  parser.add_argument(
      '--encoder_model_name',
      type=str,
      default='google/vit-large-patch32-224-in21k',
      help='Indicates decoder model name in vision encoder decoder model.'
  )
  parser.add_argument(
      '--decoder_model_name',
      type=str,
      default='gpt2-large',
      help='Indicates decoder model name in vision encoder decoder model.'
  )
  parser.add_argument(
      '--decoding_strategy',
      type=str,
      default='greedy_search',
      help='Indicates decoding strategy to use for open-ended text generation.'
  )

  args = parser.parse_args()

  print(f'Loading model from {args.model_checkpoint}')

  inference = Inference(
      decoder_model_name=args.decoder_model_name,
      model_checkpoint=args.model_checkpoint,
      decoding_strategy=args.decoding_strategy,
  )

  def generate_text(video):
      generated_text = inference.generate_text(video, args.encoder_model_name)

      return generated_text

  app = gr.Interface(fn=generate_text, inputs='video', outputs='text')
  app.launch(share=True)