import argparse


class ArgumentParser:
    def call(self):
        parser = argparse.ArgumentParser(
            description='This project is to summarize videos and search events in videos.'
        )
        parser.add_argument(
            '--model_name',
            type=str,
            required=True,
            help='Indicates current model name to be trained.'
        )
        parser.add_argument(
            '--mode',
            type=str,
            required=True,
            help='Indicates whether training or evaluation should be performed.'
        )
        parser.add_argument(
            '--encoder_model_name',
            type=str,
            default='google/vit-base-patch32-224-in21k',
            help='Indicates encoder model name in vision encoder decoder model.'
        )
        parser.add_argument(
            '--decoder_model_name',
            type=str,
            default='bert-base-cased',
            help='Indicates decoder model name in vision encoder decoder model.'
        )
        parser.add_argument(
            '--num_encoder_fine_tuned_layers',
            type=int,
            default=10,
            help='Indicates the number of layers to be fine-tuned in encoder model.'
        )
        parser.add_argument(
            '--decoder_frozen_layer_range',
            nargs='+',
            type=int,
            default=[0, 322],
            help='Indicates the range of layers to be frozen in decoder model.'
        )
        parser.add_argument(
            '--batch_size',
            type=int,
            default=30,
            help='Indicates batch size.'
        )
        parser.add_argument(
            '--num_train_epochs',
            type=int,
            default=20,
            help='Indicates number of training epochs.'
        )
        parser.add_argument(
            '--warmup_ratio',
            type=float,
            default=0.1,
            help='Indicates warmup ratio.'
        )
        parser.add_argument(
            '--num_examples',
            type=int,
            default=None,
            help='Indicates number of examples to be used.'
        )
        parser.add_argument(
            '--data_path',
            type=str,
            default='data',
            help='Indicates data path.'
        )
        parser.add_argument(
            '--output_dir',
            type=str,
            default='model_log',
            help='Indicates data path to validation sentences.'
        )
        parser.add_argument(
            '--metric_for_best_model',
            type=str,
            default='eval_loss',
            help='Indicates metric used for loading best model.'
        )
        parser.add_argument(
            '--greater_is_better',
            help='Indicates whether metric used for loading best model is better when its value is greater.',
            action='store_true'
        )
        parser.add_argument(
            '--no-greater_is_better',
            action='store_false'
        )
        parser.set_defaults(greater_is_better=False)
        parser.add_argument(
            '--evaluate_from_checkpoint',
            type=str,
            help='Indicates checkpoint path that model should be loaded from for evaluation.'
        )
        parser.add_argument(
            '--resume_from_checkpoint',
            type=str,
            default=None,
            help='Indicates whether to resume training from provided checkpoint (or last checkpoint if set to True).'
        )
        parser.add_argument(
            '--max_target_length',
            type=int,
            default=64,
            help='Indicates maximum target length in tokenizer.'
        )
        parser.add_argument(
            '--train_cross_attention',
            help='Indicates whether cross attention layers should be trained.',
            action='store_true'
        )
        parser.add_argument(
            '--no-train_cross_attention',
            action='store_false'
        )
        parser.set_defaults(greater_is_better=False)
        parser.add_argument(
            '--learning_rate',
            type=float,
            default=5e-5,
            help='Indicates learning rate for training.'
        )

        return parser.parse_args()
