# from functools import cached_property

from transformers import VisionEncoderDecoderModel


class VLM:
    def __init__(self, tokenizer, decoder_model_name, encoder_model_name, num_encoder_fine_tuned_layers=10, decoder_frozen_layer_range=(0, 322), train_cross_attention=False):
        self.encoder_model_name = encoder_model_name
        self.decoder_model_name = decoder_model_name
        self.num_encoder_fine_tuned_layers = num_encoder_fine_tuned_layers
        self.decoder_frozen_layer_range = decoder_frozen_layer_range
        self.train_cross_attention = train_cross_attention
        self.tokenizer = tokenizer

    def call(self):
        self.__init_model()

        self.__freeze_encoder()
        self.__freeze_decoder()

        self.__decorate_with_special_parameters()
        self.__decorate_with_beam_search_parameters()

        return self.encoder_decoder_model

    def __init_model(self):
        self.encoder_decoder_model = VisionEncoderDecoderModel.from_encoder_decoder_pretrained(
            self.encoder_model_name, self.decoder_model_name
        )

        return self.encoder_decoder_model

    def __freeze_decoder(self):
        if self.train_cross_attention:
            for key, param in self.encoder_decoder_model.decoder.named_parameters():
                if 'crossattention' in key or 'ln_cross_attn' in key:
                    param.requires_grad = False
            
            for _, param in self.encoder_decoder_model.decoder.lm_head.named_parameters():
                param.requires_grad = False
        else:
            start_idx, end_idx = self.decoder_frozen_layer_range

            for idx, (_, param) in enumerate(self.encoder_decoder_model.decoder.named_parameters()):
                if start_idx <= idx < end_idx:
                    param.requires_grad = False

        return self.encoder_decoder_model

    def __freeze_encoder(self):
        for idx, (_, param) in enumerate(self.encoder_decoder_model.encoder.named_parameters()):
            if idx < self.__num_of_encoder_layers() - self.num_encoder_fine_tuned_layers:
                param.requires_grad = False

        return self.encoder_decoder_model

    # @cached_property
    def __num_of_encoder_layers(self):
        return len(list(self.encoder_decoder_model.encoder.named_parameters()))

    def __decorate_with_special_parameters(self):
        # set special tokens used for creating the decoder_input_ids from the labels
        if self.tokenizer.cls_token_id:
          self.encoder_decoder_model.config.decoder_start_token_id = self.tokenizer.cls_token_id
        else:
          self.encoder_decoder_model.config.decoder_start_token_id = self.tokenizer.bos_token_id

        if not self.tokenizer.pad_token:
            self.tokenizer.add_special_tokens({'pad_token': '[PAD]'})
            self.encoder_decoder_model.decoder.resize_token_embeddings(len(self.tokenizer))

        self.encoder_decoder_model.config.pad_token_id = self.tokenizer.pad_token_id
        # make sure vocab size is set correctly
        self.encoder_decoder_model.config.vocab_size = self.encoder_decoder_model.config.decoder.vocab_size

        return self.encoder_decoder_model

    def __decorate_with_beam_search_parameters(self):
        # set beam search parameters
        self.encoder_decoder_model.config.eos_token_id = self.tokenizer.sep_token_id
        self.encoder_decoder_model.config.max_length = 32
        self.encoder_decoder_model.config.early_stopping = True
        self.encoder_decoder_model.config.no_repeat_ngram_size = 3
        self.encoder_decoder_model.config.length_penalty = 2.0
        self.encoder_decoder_model.config.num_beams = 4

        return self.encoder_decoder_model
