from tqdm import tqdm
import argparse
from datetime import timedelta
import gradio as gr
from sentence_transformers import SentenceTransformer
import torchvision
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np

from inference import Inference
import utils


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='This demo application is to demonstrate searching events in videos.'
    )
    parser.add_argument(
        '--model_checkpoint',
        type=str,
        required=True,
        help='Indicates checkpoint path that model should be loaded from for inference.'
    )
    parser.add_argument(
        '--encoder_model_name',
        type=str,
        default='google/vit-large-patch32-224-in21k',
        help='Indicates decoder model name in vision encoder decoder model.'
    )
    parser.add_argument(
        '--decoder_model_name',
        type=str,
        default='gpt2-large',
        help='Indicates decoder model name in vision encoder decoder model.'
    )
    parser.add_argument(
        '--frame_step',
        type=int,
        default=100,
        help='Indicates the number of frames to take as steps for splitting video.'
    )
    parser.add_argument(
        '--decoding_strategy',
        type=str,
        default='greedy_search',
        help='Indicates decoding strategy to use for open-ended text generation.'
  )

    args = parser.parse_args()

    print(f'Loading model from {args.model_checkpoint}')

    inference = Inference(
        decoder_model_name=args.decoder_model_name,
        model_checkpoint=args.model_checkpoint,
        decoding_strategy=args.decoding_strategy,
    )

    model = SentenceTransformer('all-mpnet-base-v2')

    def search_in_video(video, query):
        result = torchvision.io.read_video(video)
        video = result[0]
        video_fps = result[2]['video_fps']

        video_segments = [
            video[idx:idx + args.frame_step, :, :, :] for idx in range(0, video.shape[0], args.frame_step)
        ]

        generated_texts = []

        for video_seg in tqdm(video_segments):
            pixel_values = utils.video2image(video_seg, args.encoder_model_name)

            generated_text = inference.generate_text(pixel_values, args.encoder_model_name)
            generated_texts.append(generated_text)

        sentences = [query] + generated_texts

        sentence_embeddings = model.encode(sentences)

        similarities = cosine_similarity(
            [sentence_embeddings[0]],
            sentence_embeddings[1:]
        )
        arg_sorted_similarities = np.argsort(similarities)

        ordered_texts = np.array(generated_texts)[arg_sorted_similarities]
        ordered_similarity_scores = similarities[0][arg_sorted_similarities]

        texts_to_scores = dict(
            zip(ordered_texts[0].tolist(), ordered_similarity_scores[0].tolist())
        )

        print(texts_to_scores)

        best_video = video_segments[arg_sorted_similarities[0, -1]]
        torchvision.io.write_video('best.mp4', best_video, video_fps)

        total_frames = video.shape[0]

        video_frame_segs = [
            [idx, min(idx + args.frame_step, total_frames)] for idx in range(0, total_frames, args.frame_step)
        ]
        ordered_start_ends = []

        for [start, end] in video_frame_segs:
            td = timedelta(seconds=(start / video_fps))
            s = round(td.total_seconds(), 2)
            
            td = timedelta(seconds=(end / video_fps))
            e = round(td.total_seconds(), 2)
            
            ordered_start_ends.append(f'{s}:{e}')
        
        ordered_start_ends = np.array(ordered_start_ends)[arg_sorted_similarities]

        labels_to_scores = dict(
            zip(ordered_start_ends[0].tolist(), ordered_similarity_scores[0].tolist())
        )

        print('labels_to_scores', labels_to_scores)

        return 'best.mp4', labels_to_scores

    app = gr.Interface(
        fn=search_in_video,
        inputs=['video', 'text'],
        outputs=['video', gr.outputs.Label(num_top_classes=3, type='auto')],
    )
    app.launch(share=True)
