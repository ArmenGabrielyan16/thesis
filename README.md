## Pre-requesities

- Python 3.7+.
- Git large file system. It can be installed from the official [website](https://git-lfs.github.com/).
- Tested on MacOS and Linux Debian/Ubuntu.

## Installation

Follow the following steps to install the project on your environment.

1. Clone the repository `git clone git@gitlab.com:ArmenGabrielyan16/thesis.git`.
2. Change to project directory `cd thesis`.
3. Install archived datasets `git lfs pull`.
4. Extract MSR-VTT dataset `cd data && unzip msr-vtt.zip`.
5. Extract MSVD dataset `cd data && unzip msvd.zip`.
6. Install Python packages `pip install -r requirements.txt`
7. Run `python3 main.py --help` to see how the CLI application can be used for training and evaluation as well as to view what arguments it takes with descriptions.

## Training

To start the training process, you need to set `mode` argument equal to `train`. An example is provided below.

```{bash}
python3 main.py --model_name='vit-large[50]-gpt2-large[cross_attention]' \
                --mode='train' \
                --encoder_model_name='google/vit-large-patch32-224-in21k' \
                --decoder_model_name='gpt2-large' \
                --num_encoder_fine_tuned_layers=50 \
                --train_cross_attention \
                --batch_size=16 \
                --num_train_epochs=200 \
                --learning_rate=0.00005
```

## Evaluation

To turn on the evaluation mode and obtain model results, you need to set `mode` argument equal to `test`. An example is provided below.

```{bash}
python main.py --model_name='vit-base[full]-gpt2-base[cross_attention]' \
               --mode='test' \
               --encoder_model_name='google/vit-base-patch32-224-in21k' \
               --decoder_model_name='gpt2' \
               --evaluate_from_checkpoint='path to checkpoint directory' \
               --batch_size=16
```
